#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/logging/log.h>
#include <zephyr/smf.h>
#include <zephyr/drivers/adc.h> 
#include <zephyr/drivers/pwm.h>
#include "pressure.h"
#include <nrfx_power.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>


LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);

#define HEARTBEAT DT_ALIAS(heartbeat)
static const struct gpio_dt_spec heartbeat_led = GPIO_DT_SPEC_GET(HEARTBEAT, gpios);

#define LED1 DT_ALIAS(led1)
static const struct gpio_dt_spec led1 = GPIO_DT_SPEC_GET(LED1, gpios);
static const struct pwm_dt_spec led_pwm1 = PWM_DT_SPEC_GET(DT_ALIAS(pwm1));

#define LED2 DT_ALIAS(led2)
static const struct gpio_dt_spec led2 = GPIO_DT_SPEC_GET(LED2, gpios);
static const struct pwm_dt_spec led_pwm2 = PWM_DT_SPEC_GET(DT_ALIAS(pwm2));

#define ADC_DT_SPEC_GET_BY_ALIAS(vadc)                    \
{                                                            \
    .dev = DEVICE_DT_GET(DT_PARENT(DT_ALIAS(vadc))),      \
    .channel_id = DT_REG_ADDR(DT_ALIAS(vadc)),            \
    ADC_CHANNEL_CFG_FROM_DT_NODE(DT_ALIAS(vadc))          \
}															 \

#define ERROR DT_ALIAS(error)
static const struct gpio_dt_spec error_led = GPIO_DT_SPEC_GET(ERROR, gpios);

#define BUTTON1 DT_ALIAS(button1)
static const struct gpio_dt_spec button1 = GPIO_DT_SPEC_GET(BUTTON1, gpios);

#define BUTTON2 DT_ALIAS(button2)
static const struct gpio_dt_spec button2 = GPIO_DT_SPEC_GET(BUTTON2, gpios);

#define BLINK_INTERVAL_MS 1000
#define MAX_VOLT 3.3
#define MEASUREMENT_DELAY_MS 1000  // delay between measurements
#define OVERSAMPLE 10  // number of samples to average together
#define DATA_ARRAY 100
#define DATA_ARRAY2 500
#define PERIOD_ARRAY 10

void heartbeat_timer_handler(struct k_timer *heartbeat_timer);
void heartbeat_timer_work_handler(struct k_work *heartbeat_timer_work);

void led_timer_handler(struct k_timer *led_timer);
void led_timer_work_handler(struct k_work *led_timer_work);
void led_timer_stop(struct k_timer *led_timer);
void led_timer_stop_handler(struct k_work *led_timer_stop_work);

void led2_timer_handler(struct k_timer *led2_timer);
void led2_timer_work_handler(struct k_work *led2_timer_work);
void led2_timer_stop(struct k_timer *led2_timer);
void led2_timer_stop_handler(struct k_work *led2_timer_stop_work);

void error_timer_handler(struct k_timer *error_led_timer);
void error_timer_work_handler(struct k_work *error_led_timer_work);
void error_timer_stop(struct k_timer *error_led_timer);
void error_timer_stop_handler(struct k_work *error_led_timer_stop_work);

void button1_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void button1_work_handler(struct k_work *button1_work);
static struct gpio_callback button1_cb;

void button2_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins);
void button2_work_handler(struct k_work *button2_work);
static struct gpio_callback button2_cb;

void battery_read(void);
void led1_read(void);
void led2_read(void);

K_TIMER_DEFINE(heartbeat_timer, heartbeat_timer_handler, NULL);
K_WORK_DEFINE(heartbeat_timer_work, heartbeat_timer_work_handler);

K_TIMER_DEFINE(led_timer, led_timer_handler, led_timer_stop);
K_WORK_DEFINE(led_timer_work, led_timer_work_handler);
K_WORK_DEFINE(led_timer_stop_work, led_timer_stop_handler);

K_TIMER_DEFINE(led2_timer, led2_timer_handler, led2_timer_stop);
K_WORK_DEFINE(led2_timer_work, led2_timer_work_handler);
K_WORK_DEFINE(led2_timer_stop_work, led2_timer_stop_handler);

K_TIMER_DEFINE(error_timer, error_timer_handler, error_timer_stop);
K_WORK_DEFINE(error_timer_work, error_timer_work_handler);
K_WORK_DEFINE(error_timer_stop_work, error_timer_stop_handler);

K_WORK_DEFINE(button1_work, button1_work_handler);
K_WORK_DEFINE(button2_work, button2_work_handler);

static int init(int *o);
static void idle_entry(void *o);
static void idle_run(void *o);
static void action_entry(void *o);
static void action_run(void *o);
static void p_error_entry(void *o);
static void p_error_run(void *o);
static void v_error_entry(void *o);
static void v_error_run(void *o);

int8_t ret;
int8_t err;
int32_t battery;
float ptp1;
float ptp2;
int16_t brightness1;
int16_t brightness2;
int32_t val_mv;
int32_t val_mv1;
int32_t val_mv2;
static int32_t atm_pressure_kPa;
bool usbregstatus;
int i;
int j;
int a;
int b;
int period;
int period2;
float battery_level;
int normalized_level;

float raw_data_array1[DATA_ARRAY]={};
float raw_data_array2[DATA_ARRAY2]={};
float max_data_array1[PERIOD_ARRAY]={};
float min_data_array1[PERIOD_ARRAY]={};
float max_data_array2[PERIOD_ARRAY]={};
float min_data_array2[PERIOD_ARRAY]={};

enum data_outputs {LED1_Brightness, LED2_Brightness, Pressure};
int16_t data_output[3];
/******************************************************************************************/
//BLUETOOTH STUFF

/* Define macros for UUIDs of the Remote Service
   Project ID: 001 (3rd entry)
   MFG ID = 0x01FF (4th entry)
*/
#define BT_UUID_REMOTE_SERV_VAL \
        BT_UUID_128_ENCODE(0xe9ea0000, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_SERVICE 			BT_UUID_DECLARE_128(BT_UUID_REMOTE_SERV_VAL)

/* UUID of the Data Characteristic */
#define BT_UUID_REMOTE_DATA_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0001, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_DATA_CHRC 		BT_UUID_DECLARE_128(BT_UUID_REMOTE_DATA_CHRC_VAL)

/* UUID of the Message Characteristic */
#define BT_UUID_REMOTE_MESSAGE_CHRC_VAL \
        BT_UUID_128_ENCODE(0xe9ea0002, 0xe19b, 0x0001, 0x01FF, 0xc7907585fc48)
#define BT_UUID_REMOTE_MESSAGE_CHRC 	BT_UUID_DECLARE_128(BT_UUID_REMOTE_MESSAGE_CHRC_VAL) 

#define DEVICE_NAME CONFIG_BT_DEVICE_NAME
#define DEVICE_NAME_LEN (sizeof(DEVICE_NAME)-1)
#define BLE_DATA_POINTS 600 // limited by MTU

// enumeration to keep track of the state of the notifications
enum bt_data_notifications_enabled {
    BT_DATA_NOTIFICATIONS_ENABLED,
    BT_DATA_NOTIFICATIONS_DISABLED,
};
enum bt_data_notifications_enabled notifications_enabled;

// declare struct of the BLE remove service callbacks
struct bt_remote_srv_cb {
    void (*notif_changed)(enum bt_data_notifications_enabled status);
    void (*data_rx)(struct bt_conn *conn, const uint8_t *const data, uint16_t len);
}; 
struct bt_remote_srv_cb remote_service_callbacks;

// blocking thread semaphore to wait for BLE to initialize
static K_SEM_DEFINE(bt_init_ok, 1, 1);

/* Advertising data */
static const struct bt_data ad[] = {
    BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
    BT_DATA(BT_DATA_NAME_COMPLETE, DEVICE_NAME, DEVICE_NAME_LEN)
};

/* Scan response data */
static const struct bt_data sd[] = {
    BT_DATA_BYTES(BT_DATA_UUID128_ALL, BT_UUID_REMOTE_SERV_VAL), 
};

/* Function Declarations */
static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset);
void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value);
static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags);
void bluetooth_set_battery_level(int level, int nominal_batt_mv);
uint8_t bluetooth_get_battery_level(void);

/* Function Declarations */
static struct bt_conn *current_conn;
void on_connected(struct bt_conn *conn, uint8_t ret);
void on_disconnected(struct bt_conn *conn, uint8_t reason);
void on_notif_changed(enum bt_data_notifications_enabled status);
void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len);

 /* Setup BLE Services */
BT_GATT_SERVICE_DEFINE(remote_srv,
BT_GATT_PRIMARY_SERVICE(BT_UUID_REMOTE_SERVICE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_DATA_CHRC,
                 BT_GATT_CHRC_READ | BT_GATT_CHRC_NOTIFY,
                 BT_GATT_PERM_READ,
                 read_data_cb, NULL, NULL),
BT_GATT_CCC(data_ccc_cfg_changed_cb, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE),
BT_GATT_CHARACTERISTIC(BT_UUID_REMOTE_MESSAGE_CHRC,
                 BT_GATT_CHRC_WRITE_WITHOUT_RESP,
                 BT_GATT_PERM_WRITE,
                 NULL, on_write, NULL),
);

/* Setup Callbacks */
struct bt_conn_cb bluetooth_callbacks = {
    .connected = on_connected,
    .disconnected = on_disconnected,
};

struct bt_remote_srv_cb remote_service_callbacks = {
    .notif_changed = on_notif_changed,
    .data_rx = on_data_rx,
};

/******************************************************************************************/
  
static const struct adc_dt_spec vadc_battery = ADC_DT_SPEC_GET_BY_ALIAS(vadc);
static const struct adc_dt_spec vadc1 = ADC_DT_SPEC_GET_BY_ALIAS(vadc1);
static const struct adc_dt_spec vadc2 = ADC_DT_SPEC_GET_BY_ALIAS(vadc2);

int16_t buf;
struct adc_sequence sequence = {
		.buffer = &buf,
	.buffer_size = sizeof(buf), // bytes
};

int16_t buf1;
struct adc_sequence sequence1 = {
		.buffer = &buf1,
	.buffer_size = sizeof(buf1), // bytes
};

int16_t buf2;
struct adc_sequence sequence2 = {
		.buffer = &buf2,
	.buffer_size = sizeof(buf2), // bytes
};

const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr);

static const struct smf_state states[];
enum state{Init, Idle, Action, Pressure_Error, VBUS_Error};
struct s_object {
        /* This must be first */
        struct smf_ctx ctx;
} s_obj;

static const struct smf_state states[] = {
        [Init] = SMF_CREATE_STATE(init, NULL, NULL),
		[Idle] = SMF_CREATE_STATE(idle_entry, idle_run, NULL),
		[Action]= SMF_CREATE_STATE(action_entry, action_run, NULL),
		[Pressure_Error] = SMF_CREATE_STATE(p_error_entry, p_error_run, NULL),
		[VBUS_Error] = SMF_CREATE_STATE(v_error_entry, v_error_run, NULL),
};

/******************************************************************************************/

static int init(int *o)
{	
	LOG_INF("INIT ENTRY");

	if (!device_is_ready(heartbeat_led.port))
	{
		return -1;
	}

	ret = gpio_pin_configure_dt(&heartbeat_led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

    ret = gpio_pin_configure_dt(&led1, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

    ret = gpio_pin_configure_dt(&led2, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

    ret = gpio_pin_configure_dt(&error_led, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

    ret = gpio_pin_configure_dt(&button1, GPIO_INPUT);
	if(ret<0){
		return ret;
	}

    ret = gpio_pin_configure_dt(&button2, GPIO_INPUT);
	if(ret<0){
		return ret;
	}

	/* Configure the ADC channel */
	err = adc_channel_setup_dt(&vadc_battery);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel (%d)", err);
		return err;
	}

	err = adc_channel_setup_dt(&vadc1);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel (%d)", err);
		return err;
	}

	err = adc_channel_setup_dt(&vadc2);
	if (err < 0) {
		LOG_ERR("Could not setup ADC channel (%d)", err);
		return err;
	}

	err = pwm_set_pulse_dt(&led_pwm1, led_pwm1.period); 
	if (err < 0) {
    	LOG_ERR("Could not set led driver 1 (PWM0)");
		return err;
	}

	 /* Initialize Bluetooth */
    int err = bluetooth_init(&bluetooth_callbacks, &remote_service_callbacks);
    if (err) {
        LOG_ERR("BT init failed (err = %d)", err);
    }

	if (!device_is_ready(pressure_in)) {
		LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
		return;
	}
    else {
        LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
    }

	LOG_INF("HB ON");
	k_timer_start(&heartbeat_timer, K_MSEC(BLINK_INTERVAL_MS), K_MSEC(BLINK_INTERVAL_MS));

	smf_set_state(SMF_CTX(&s_obj), &states[Idle]);
}

static void idle_entry(void *o)
{
	LOG_INF("IDLE ENTRY");
	gpio_pin_set_dt(&led1, 0);
	gpio_pin_set_dt(&led2, 0);
	gpio_pin_set_dt(&error_led,0);

	(void)adc_sequence_init_dt(&vadc_battery, &sequence);
	(void)adc_sequence_init_dt(&vadc1, &sequence1);
	(void)adc_sequence_init_dt(&vadc2, &sequence2);

	//initializing buttons
    ret = gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_EDGE_TO_ACTIVE);
	gpio_init_callback(&button1_cb, button1_handler, BIT(button1.pin));	
	gpio_add_callback(button1.port, &button1_cb);

    ret = gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_EDGE_TO_ACTIVE);
	gpio_init_callback(&button2_cb, button2_handler, BIT(button2.pin));	
	gpio_add_callback(button2.port, &button2_cb);
}

static void idle_run(void *o)
{
	//LOG_INF("IDLE RUN");

	float atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
	data_output[Pressure] = atm_pressure_kPa;
	k_msleep(MEASUREMENT_DELAY_MS);

	if (atm_pressure_kPa <= 0)
    {
        smf_set_state(SMF_CTX(&s_obj), &states[Pressure_Error]);
    } 

	usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
	if (usbregstatus==true) 
	{
    	// VBUS detected --> stop all measurement functions & turn on LED blinking at 1hz
		LOG_INF("VBUS Detected");
		smf_set_state(SMF_CTX(&s_obj), &states[VBUS_Error]);
	} 

	k_msleep(100);
}

static void action_entry(void *o)
{
	LOG_INF("ACTION ENTRY");
	
}

static void action_run(void *o)
{
	//LOG_INF("ACTION RUN");
	k_timer_start(&led_timer, K_MSEC(100), K_MSEC(100));
	k_timer_start(&led2_timer,K_MSEC(100), K_MSEC(100));

	atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
	//LOG_INF("Pressure: %lld", atm_pressure_kPa);
}

static void p_error_entry(void *o)
{
	LOG_INF("ERROR ENTRY");
	gpio_pin_set_dt(&led1, 0);
	gpio_pin_set_dt(&led2, 0);
	gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_DISABLE);
	gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_DISABLE);
}

static void p_error_run(void *o)
{
	float atm_pressure_kPa = read_pressure_sensor(pressure_in, OVERSAMPLE, 0);
	if(atm_pressure_kPa <= 0)
	{
		LOG_ERR("ERROR - Presure sensor not connected");
		gpio_pin_set_dt(&error_led, 1);
	}
    else 
	{
        LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
		gpio_pin_set_dt(&error_led, 0);
		smf_set_state(SMF_CTX(&s_obj), &states[Idle]);
    }
}

static void v_error_entry(void *o)
{
	LOG_INF("ERROR ENTRY");
	gpio_pin_set_dt(&led1, 0);
	gpio_pin_set_dt(&led2, 0);
	gpio_pin_interrupt_configure_dt(&button1, GPIO_INT_DISABLE);
	gpio_pin_interrupt_configure_dt(&button2, GPIO_INT_DISABLE);
}

static void v_error_run(void *o)
{
	usbregstatus = nrf_power_usbregstatus_vbusdet_get(NRF_POWER);
	if (usbregstatus==true) 
	{
    	// VBUS detected --> stop all measurement functions & turn on LED blinking at 1hz
		LOG_ERR("ERROR - VBUS detected");
		k_timer_start(&error_timer, K_MSEC(BLINK_INTERVAL_MS), K_MSEC(BLINK_INTERVAL_MS));
	} 
	else {
    	// VBUS not detected --> resume measurements
		smf_set_state(SMF_CTX(&s_obj), &states[Idle]);
	}
}

/******************************************************************************************/
//Bluetooth Functions

void data_ccc_cfg_changed_cb(const struct bt_gatt_attr *attr, uint16_t value) {
    bool notif_enabled = (value == BT_GATT_CCC_NOTIFY);
    LOG_INF("Notifications: %s", notif_enabled? "enabled":"disabled");

    notifications_enabled = notif_enabled? BT_DATA_NOTIFICATIONS_ENABLED:BT_DATA_NOTIFICATIONS_DISABLED;

    if (remote_service_callbacks.notif_changed) {
        remote_service_callbacks.notif_changed(notifications_enabled);
    }
}

static ssize_t read_data_cb(struct bt_conn *conn, const struct bt_gatt_attr *attr, void *buf, uint16_t len, uint16_t offset) {
    return bt_gatt_attr_read(conn, attr, buf, len, offset, &data_output, sizeof(data_output));
}

static ssize_t on_write(struct bt_conn *conn, const struct bt_gatt_attr *attr, const void *buf, uint16_t len, uint16_t offset, uint8_t flags) {
    LOG_INF("Received data, handle %d, conn %p", attr->handle, (void *)conn);
    
    if (remote_service_callbacks.data_rx) {
        remote_service_callbacks.data_rx(conn, buf, len);
    }
    return len;
}

void on_sent(struct bt_conn *conn, void *user_data) {
    ARG_UNUSED(user_data);
    LOG_INF("Notification sent on connection %p", (void *)conn);
}

void bt_ready(int ret) {
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
    }

    // release the thread once initialized
    k_sem_give(&bt_init_ok);
}

int send_data_notification(struct bt_conn *conn, uint8_t *value, uint16_t length) {
    int ret = 0;

    struct bt_gatt_notify_params params = {0};
    const struct bt_gatt_attr *attr = &remote_srv.attrs[2];

    params.attr = attr;
    params.data = &value;
    params.len = length;
    params.func = on_sent;

    ret = bt_gatt_notify_cb(conn, &params);

    return ret;
}

/* Initialize the BLE Connection */
int bluetooth_init(struct bt_conn_cb *bt_cb, struct bt_remote_srv_cb *remote_cb) {
    LOG_INF("Initializing Bluetooth");

    if ((bt_cb == NULL) | (remote_cb == NULL)) {
        return -NRFX_ERROR_NULL;
    }
    bt_conn_cb_register(bt_cb);
    remote_service_callbacks.notif_changed = remote_cb->notif_changed;
    remote_service_callbacks.data_rx = remote_cb->data_rx;

    int ret = bt_enable(bt_ready);
    if (ret) {
        LOG_ERR("bt_enable returned %d", ret);
        return ret;
    }

	// hold the thread until Bluetooth is initialized
	k_sem_take(&bt_init_ok, K_FOREVER);

	if (IS_ENABLED(CONFIG_BT_SETTINGS)) {
		settings_load();
	}

	ret = bt_le_adv_start(BT_LE_ADV_CONN, ad, ARRAY_SIZE(ad), sd, ARRAY_SIZE(sd));
	if (ret) {
		LOG_ERR("Could not start advertising (ret = %d)", ret);
		return ret;
	}

	return ret;
}

void on_data_rx(struct bt_conn *conn, const uint8_t *const data, uint16_t len) {
    uint8_t temp_str[len+1];
    memcpy(temp_str, data, len);
    temp_str[len] = 0x00; // manually append NULL character at the end

    LOG_INF("BT received data on conn %p. Len: %d", (void *)conn, len);
    LOG_INF("Data: %s", temp_str);
}

void on_connected(struct bt_conn *conn, uint8_t ret) {
    if (ret) { LOG_ERR("Connection error: %d", ret); }
    LOG_INF("BT connected");
    current_conn = bt_conn_ref(conn);
}

void on_disconnected(struct bt_conn *conn, uint8_t reason) {
    LOG_INF("BT disconnected (reason: %d)", reason);
    if (current_conn) {
        bt_conn_unref(current_conn);
        current_conn = NULL;
    }
}

void on_notif_changed(enum bt_data_notifications_enabled status) {
    if (status == BT_DATA_NOTIFICATIONS_ENABLED) {
        LOG_INF("BT notifications enabled");
    }
    else {
        LOG_INF("BT notifications disabled");
    }
}
/******************************************************************************************/

void heartbeat_timer_handler(struct k_timer *heartbeat_timer)
{
	k_work_submit(&heartbeat_timer_work);
	//LOG_INF("Submitted heartbeat led work to the queue! (%lld)", k_uptime_get());
}

//toggle hearbeat led - timer on 1000MS interval w/ no stop
void heartbeat_timer_work_handler(struct k_work *heartbeat_timer_work)
{
	gpio_pin_toggle_dt(&heartbeat_led);
}

void led_timer_handler(struct k_timer *led_timer)
{
	//LOG_INF("Submit Action LEDs Timer Work");
	k_work_submit(&led_timer_work);
}

void led_timer_work_handler(struct k_work *led_timer_work)
{
	//LOG_INF("Doing LED Timer Work");
    //calculations for LED1 and LED 2   

	if (i < DATA_ARRAY) {
		led1_read();
		raw_data_array1[i++] = val_mv1;
	}
	else if(i = DATA_ARRAY) //if array is filled with 100 values stop timer
	{ 
		LOG_INF("Time has run out for one run");
		k_timer_stop(&led_timer);

		for (int i = 0; i < (DATA_ARRAY); i++) {
		LOG_INF("array_data_led1[%d] = %f", i, raw_data_array1[i]);
		}
	}
	
}

void led_timer_stop(struct k_timer *led_timer)
{
	k_work_submit(&led_timer_stop_work);
	//LOG_INF("Submitted stop action led work to the queue!");
}

void led_timer_stop_handler(struct k_work *led_timer_stop_work)
{
	//LOG_INF("LED OFF\n");
	LOG_INF("LED 1 STOP HANDLER");

	float min_value1;
	float max_value1;
	float max_sum = 0; 
	float max_ave;
	float min_sum = 0;
	float min_ave;

	if(period < PERIOD_ARRAY){
		// Iterate through the array to find min and max
		LOG_INF("period: %d", period);
		period++;
		
		min_value1 = raw_data_array1[i];
		max_value1 = raw_data_array1[i];

		for (int j = 0; j < DATA_ARRAY; j++) 
		{
			if (raw_data_array1[j] < min_value1) 
			{
				min_value1 = raw_data_array1[j];
				//LOG_INF("Min Value: %f", min_value1);
			}       
			if (raw_data_array1[j] > max_value1) 
			{
				max_value1 = raw_data_array1[j];
				//LOG_INF("Max Value: %f", max_value1);
			}
		}

		LOG_INF("LED 1 Max Value: %f", max_value1);
		LOG_INF("LED 1 Min Value: %f", min_value1);

		
		max_data_array1[j] = max_value1;
		min_data_array1[j] = min_value1;
		j++;

		i = 0;
		k_timer_start(&led_timer, K_MSEC(100), K_MSEC(100));
	}
	else if (period = PERIOD_ARRAY)
	{
		LOG_INF("Calculate average of max and min of LED 1");
		
		// for (int i = 0; i < (DATA_ARRAY); i++) {
       	// 	 LOG_INF("max_data_led1[%d] = %f", i, max_data_array1[i]);
		// 	 //LOG_INF("min_data_led1[%d] = %f", i, min_data_array1[i] );
		// }

		for (int i = 0; i < DATA_ARRAY; i++){
			max_sum += max_data_array1[i];
			//LOG_INF("Sum of Max Values: %f", max_sum);
		}

		max_ave = max_sum/DATA_ARRAY;
		LOG_INF("LED 1 Average Max Value: %f", max_ave);

		for (int i = 0; i < DATA_ARRAY; i++){
			min_sum += min_data_array1[i];
			//LOG_INF("Sum of Min Values: %f", min_sum);
		}
		
		min_ave = min_sum/DATA_ARRAY;
		LOG_INF("LED 1 Average Min Value: %f", min_ave);

		ptp1 = max_ave - min_ave;
		LOG_INF("LED 1 Peak to Peak Value: %f", ptp1);

		brightness1 = (1000000*(ptp1-(-5))/(50-(-5)));
		LOG_INF("LED 1 Brightness Percentange: %f", ((float)brightness1/10000));
		pwm_set_pulse_dt(&led_pwm1, brightness1);

		smf_set_state(SMF_CTX(&s_obj), &states[Idle]);
	}
}

void led2_timer_handler(struct k_timer *led2_timer)
{
	//LOG_INF("Submit Action LEDs Timer Work");
	k_work_submit(&led2_timer_work);
}

void led2_timer_work_handler(struct k_work *led2_timer_work)
{
	//LOG_INF("Doing LED Timer Work");
    //calculations for LED1 and LED 2   

	if (a < DATA_ARRAY2) {
		led2_read();
		raw_data_array2[a++] = val_mv2;
	}
	else if(a = DATA_ARRAY2) //if array is filled with 100 values stop timer
	{ 
		LOG_INF("Time has run out for one run");
		k_timer_stop(&led2_timer);

		for (int a = 0; a < (DATA_ARRAY2); a++) {
		LOG_INF("array_data_led2[%d] = %f", a, raw_data_array2[a]);
		}
	}
}

void led2_timer_stop(struct k_timer *led2_timer)
{
	k_work_submit(&led2_timer_stop_work);
	//LOG_INF("Submitted stop action led work to the queue!");
}

void led2_timer_stop_handler(struct k_work *led2_timer_stop_work)
{
	//LOG_INF("LED OFF\n");
	float min_value2;
	float max_value2;
	float max_sum2 = 0; 
	float max_ave2;
	float min_sum2 = 0;
	float min_ave2;

	LOG_INF("LED 2 STOP HANDLER");

	if(period2 < PERIOD_ARRAY){
		// Iterate through the array to find min and max
		LOG_INF("period2: %d", period2);
		period2++;
		
		min_value2 = raw_data_array2[a];
		max_value2 = raw_data_array2[a];

		for (int b = 0; b < DATA_ARRAY2; b++) 
		{
			if (raw_data_array2[b] < min_value2) 
			{
				min_value2 = raw_data_array2[b];
				//LOG_INF("Min Value: %f", min_value1);
			}       
			if (raw_data_array2[b] > max_value2) 
			{
				max_value2 = raw_data_array2[b];
				//LOG_INF("Max Value: %f", max_value1);
			}
		}

		LOG_INF("LED 2 Max Value: %f", max_value2);
		LOG_INF("LED 2 Min Value: %f", min_value2);

		
		max_data_array2[b] = max_value2;
		min_data_array2[b] = min_value2;
		b++;

		a = 0;
		k_timer_start(&led2_timer, K_MSEC(100), K_MSEC(100));
	}
	else if (period2 = PERIOD_ARRAY)
	{
		LOG_INF("Calculate average of max and min of LED 2");
		
		// for (int a = 0; a < (DATA_ARRAY); a++) {
       	// 	 LOG_INF("max_data_led2[%d] = %f", 2, max_data_array2[a]);
		// 	 //LOG_INF("min_data_led2[%d] = %f", a, min_data_array2[a] );
		// }

		for (int a = 0; a < DATA_ARRAY2; a++){
			max_sum2 += max_data_array2[a];
			//LOG_INF("Sum of Max Values: %f", max_sum);
		}

		max_ave2 = max_sum2/DATA_ARRAY2;
		LOG_INF("LED 2 Average Max Value: %f", max_ave2);

		for (int a = 0; a < DATA_ARRAY2; a++){
			min_sum2 += min_data_array2[a];
			//LOG_INF("Sum of Min Values: %f", min_sum);
		}
		
		min_ave2 = min_sum2/DATA_ARRAY2;
		LOG_INF("LED 2 Average Min Value: %f", min_ave2);

		ptp2 = max_ave2 - min_ave2;
		LOG_INF("LED 2 Peak to Peak Value: %f", ptp2);

		brightness2 = (1000000*(ptp2-(-10))/(150-(-10)));
		LOG_INF("LED 2 Brightness Percentange: %d", ((float)brightness2/10000));
		pwm_set_pulse_dt(&led_pwm2,brightness2);

		smf_set_state(SMF_CTX(&s_obj), &states[Idle]);
	}
}

void error_timer_handler(struct k_timer *error_led_timer)
{
	k_work_submit(&error_timer_work);
}
void error_timer_work_handler(struct k_work *error_led_timer_work)
{
	gpio_pin_toggle_dt(&error_led);
}
void error_timer_stop(struct k_timer *error_led_timer)
{
	k_work_submit(&error_timer_stop_work);
}
void error_timer_stop_handler(struct k_work *error_led_timer_stop_work)
{
	gpio_pin_set_dt(&error_led, 0);
}

void button1_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
	LOG_INF("Submitting button 1 work");
	k_work_submit(&button1_work);
}

void button1_work_handler(struct k_work *button1_work)
{
	LOG_INF("Doing button 1 work");
	period = 0;
	period2 = 0;
	data_output[LED1_Brightness]=0;
	data_output[LED2_Brightness]=0;
	data_output[Pressure]=0;

	smf_set_state(SMF_CTX(&s_obj), &states[Action]);
}

void button2_handler(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
	k_work_submit(&button2_work);
}

void button2_work_handler(struct k_work *button1_work)
{
	LOG_INF("Doing button 2 work!");
	// trigger bluetooth notification
	
	//READ BATTERY
	battery_read();
    float battery_level = ((val_mv)/36);
    LOG_INF("Battery Percentage: %f", battery_level);

	int bat_err;
    if (bat_err) {
        LOG_ERR("BT init failed (err = %d)", bat_err);
    }

	bat_err = bt_bas_set_battery_level(normalized_level);
    if (bat_err) {
        LOG_ERR("BAS set error (err = %d)",bat_err);
    }

    // this function retrieves BAS GATT with information
    battery_level =  bt_bas_get_battery_level();

	//used these values to check if bluetooth was working with hard coded values
	// data_output[LED1_Brightness]=50;
	// data_output[LED2_Brightness]=90;
	// data_output[Pressure]=102;

	LOG_INF("LED1 Brightness :%d",data_output[LED1_Brightness]);
	LOG_INF("LED2 Brightness :%d",data_output[LED2_Brightness]);
	LOG_INF("Pressure:%d",data_output[Pressure]);

	int bat_err1;
	bat_err1 = send_data_notification(current_conn, data_output, 3);
    if (bat_err1=0) {
        LOG_ERR("Could not send BT notification (err: %d)", bat_err1);
    }
    else {
        LOG_INF("BT data transmitted.");
    }
}

void battery_read(void)
{
	//CHANNEL 0
	int ret0;
	ret0 = adc_read(vadc_battery.dev, &sequence);
	if (ret0 < 0)
	{
   	 	LOG_ERR("Could not read (%d)", ret0);
	} 
	else
	{
    	LOG_DBG("Raw ADC Buffer: %d", buf);
	}
	
	val_mv = buf;
	ret0 = adc_raw_to_millivolts_dt(&vadc_battery, &val_mv); // remember that the vadc struct containts all the DT parameters
	
	if (ret0 < 0) 
	{
    	LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
	} 
	else 
	{
    	LOG_INF("ADC Value (mV): %d", val_mv);
	}
}

void led1_read(void)
{
	//CHANNEL 1
    int ret1;
    ret1 = adc_read(vadc1.dev, &sequence1);
    if (ret1 < 0) 
    {
        LOG_ERR("Could not read (%d)", ret1);
    } 

    else 
	{
		//LOG_DBG("Raw ADC Buffer: %d", buf1);
	}

    val_mv1 = buf1;
    ret1 = adc_raw_to_millivolts_dt(&vadc1, &val_mv1); // remember that the vadc struct containts all the DT parameters
    if (ret1 < 0) 
    {
    	LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } 
    else 
	{
		//LOG_INF("ADC Value (mV): %d", val_mv1);
	}
}

void led2_read(void)
{
	//CHANNEL 2
	int ret2;
    ret2 = adc_read(vadc2.dev, &sequence2);
    if (ret2 < 0) 
    {
    	LOG_ERR("Could not read (%d)", ret2);
    } 
    else 
	{
		//LOG_DBG("Raw ADC Buffer: %d", buf2);
	}

    val_mv2 = buf2;
    ret2 = adc_raw_to_millivolts_dt(&vadc2, &val_mv2); // remember that the vadc struct containts all the DT parameters
    if (ret2 < 0) 
    {
        LOG_ERR("Buffer cannot be converted to mV; returning raw buffer value.");
    } 
    else 
	{
		//LOG_INF("ADC Value (mV): %d", val_mv2);
	}
}

/******************************************************************************************/

void main(void) {

	smf_set_initial(SMF_CTX(&s_obj), &states[Init]);

	 while(1) {
        /* State machine terminates if a non-zero value is returned */
         ret = smf_run_state(SMF_CTX(&s_obj));
         if (ret) 
		 {
             /* handle return code and terminate state machine */
             break;
         }	

        k_msleep(1000);
	 }
}